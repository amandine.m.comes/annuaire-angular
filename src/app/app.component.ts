import { Component } from '@angular/core';
import axios from 'axios';

interface User {
  picture: string;
  firstName: string;
  lastName: string;
  age: number;
  streetNumber: number;
  streetName: string;
  postCode: number;
  city: string;
  mail: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  public title: string = 'Annuaire aléatoire';
  public users: User[] = [];

  public addUser(): void {
    axios.get("https://randomuser.me/api/")
      .then((response) => {
        const api = response.data.results[0];

        this.users.push({
          picture: api.picture.large,
          firstName: api.name.first,
          lastName: api.name.last,
          age: api.dob.age,
          streetNumber: api.location.street.number,
          streetName: api.location.street.name,
          postCode: api.location.postcode,
          city: api.location.city,
          mail: api.email,
        })

      })
      .catch(error => {
        console.log(error);
      })

  }
}
